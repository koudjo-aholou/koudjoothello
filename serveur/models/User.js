const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  jeux_id: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'game'
  }]

})

// eslint-disable-next-line no-undef
module.exports = User = mongoose.model('user', UserSchema)
