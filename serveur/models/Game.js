const mongoose = require('mongoose')
const Schema = mongoose.Schema

// 1 Blanc
// -1 Noir

const GameSchema = new Schema({
  board: [
  ],
  score: {
    playerone: { type: Number, required: true },
    playertwo: { type: Number, required: true },
    currentPlayer: { type: String, required: true }
  },
  playerone: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  }
  // playertwo: {
  //   type: Schema.Types.ObjectId,
  //   ref: 'users'
  // },
})

// eslint-disable-next-line no-undef
module.exports = Game = mongoose.model('game', GameSchema)
