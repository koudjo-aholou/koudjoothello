const express = require('express')
const router = express.Router()
const { check, validationResult } = require('express-validator')
const auth = require('../../middleware/auth')

const Game = require('../../models/Game')
// const User = require('../../models/User')

// initialisation du jeu othello   POST api/games

router.post(
  '/',
  [
    auth,
    [
      check('playerone', ' ID Playerone is required')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    try {
      //  const user = await User.findById(req.user.id).select('-password')

      const newGame = new Game({
        board: req.body.board,
        score: req.body.score,
        playerone: req.user.id
      })

      const game = await newGame.save()

      res.json({ game })
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server Error')
    }
  }
)

// @route    GET api/games

router.get('/', auth, async (req, res) => {
  try {
    const games = await Game.find()
    res.json(games)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//    POST api/games/game afficher un post

router.post('/game', auth, async (req, res) => {
  try {
    // console.log(req.body, '--------')
    const game = await Game.findById(req.body._id).populate({
      path: 'playerone',
      model: 'user'
    })

    // Check for ObjectId format and post
    if (!req.body._id.match(/^[0-9a-fA-F]{24}$/) || !game) {
      return res.status(404).json({ msg: 'Game not found' })
    }
    res.json({ game })
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

// mettre a jour la partie  PUT api/games/

router.put('/game', auth, async (req, res) => {
  const score = req.body.score
  const id = req.body._id
  const board = req.body.board

  try {
    const game = await Game.findOneAndUpdate({ _id: id }, { $set: { score: score, board: board } }, { new: true })

    res.json({ data: game })
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

module.exports = router
