import React from 'react'
import TestRenderer from 'react-test-renderer'
import App from './App'

describe('App', () => {
  it('Checked App render correctly', async () => {
    const component = TestRenderer.create(
      <App />
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})
