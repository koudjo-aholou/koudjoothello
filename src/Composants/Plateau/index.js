import React, { useState, useEffect, useRef } from 'react'
import Jeu from './Com/Jeu-state'
import GameOver from './Com/GameOver'
import { useParams } from 'react-router'
import axios from 'axios'

function Plateau () {
  const { id } = useParams()
  const idGame = id
  const identification = localStorage.getItem('token')
  const [err, setErr] = useState(false)
  console.log(err)
  // const [plateauServeur, setPlateauServeur] = useState('')
  const didMountRef = useRef(false)
  const [tabDeBord, setTabDeBord] = useState({})
  const [userName, setUserName] = useState('')

  useEffect(() => {
    if (!didMountRef.current) {
      didMountRef.current = true
      fetchData()
    }// react-hooks/exhaustive-deps linter
  }, []) // eslint-disable-line 

  async function fetchData () {
    const formData = {
      _id: idGame
    }

    const config = {
      headers: {
        'Content-Type': 'application/json',
        'X-Auth-Token': identification
      }
    }

    await axios.post('http://localhost:5000/api/games/game', formData, config)
      .then(res => {
        setTabDeBord({
          status: 'active',
          winner: null,
          whiteScore: res.data.game.score.playertwo,
          blackScore: res.data.game.score.playerone
        })
        // setPlateauServeur(res.data.game.board)
        setUserName(res.data.game.playerone.name)
      })
      .catch(err => setErr(err))
  }

  // console.log(tabDeBord, 'tab de bord', plateauServeur, 'plateau serveur')
  const recommencerJeu = () => {
    setTabDeBord({
      ...tabDeBord,
      status: 'active'
    })
  }

  const finDeLaPartie = (winner, whiteScore, blackScore) => {
    setTabDeBord({
      status: 'over',
      winner: winner,
      whiteScore: whiteScore,
      blackScore: blackScore
    })
  }

  let game
  if (tabDeBord.status === 'active') {
    game =
      <Jeu
        end={ (winner, whiteScore, blackScore) => finDeLaPartie(winner, whiteScore, blackScore)}
        idGame={idGame}
        playerName={userName}
      />
  }

  let gameOver
  if (tabDeBord.status === 'over') {
    gameOver =
      <GameOver
        winner={tabDeBord.winner}
        restart={ () => recommencerJeu()}
        white={tabDeBord.whiteScore}
        black={tabDeBord.blackScore}
      />
  }

  return (
    <main className="App">
      {game}
      {gameOver}
    </main>
  )
}

export default Plateau
