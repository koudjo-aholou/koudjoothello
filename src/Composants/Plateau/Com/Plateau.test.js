import React from 'react'
import TestRenderer from 'react-test-renderer'
import Plateau from './Plateau'

describe('Point', () => {
  it('Checked Point render correctly', async () => {
    const component = TestRenderer.create(
      <Plateau board ={board}/>
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})

const board = [
  [
    {
      id: 1,
      disk: null,
      canReverse: []
    },
    {
      id: 2,
      disk: null,
      canReverse: []
    },
    {
      id: 3,
      disk: null,
      canReverse: []
    },
    {
      id: 4,
      disk: null,
      canReverse: []
    },
    {
      id: 5,
      disk: null,
      canReverse: []
    },
    {
      id: 6,
      disk: null,
      canReverse: []
    },
    {
      id: 7,
      disk: null,
      canReverse: []
    },
    {
      id: 8,
      disk: null,
      canReverse: []
    }
  ],
  [
    {
      id: 9,
      disk: null,
      canReverse: []
    },
    {
      id: 10,
      disk: null,
      canReverse: []
    },
    {
      id: 11,
      disk: null,
      canReverse: []
    },
    {
      id: 12,
      disk: null,
      canReverse: []
    },
    {
      id: 13,
      disk: null,
      canReverse: []
    },
    {
      id: 14,
      disk: null,
      canReverse: []
    },
    {
      id: 15,
      disk: null,
      canReverse: []
    },
    {
      id: 16,
      disk: null,
      canReverse: []
    }
  ],
  [
    {
      id: 17,
      disk: null,
      canReverse: []
    },
    {
      id: 18,
      disk: null,
      canReverse: []
    },
    {
      id: 19,
      disk: null,
      canReverse: []
    },
    {
      id: 20,
      disk: null,
      canReverse: []
    },
    {
      id: 21,
      disk: null,
      canReverse: []
    },
    {
      id: 22,
      disk: null,
      canReverse: []
    },
    {
      id: 23,
      disk: null,
      canReverse: []
    },
    {
      id: 24,
      disk: null,
      canReverse: []
    }
  ],
  [
    {
      id: 25,
      disk: null,
      canReverse: []
    },
    {
      id: 26,
      disk: null,
      canReverse: []
    },
    {
      id: 27,
      disk: null,
      canReverse: []
    },
    {
      id: 28,
      disk: 'black',
      canReverse: []
    },
    {
      id: 29,
      disk: 'null',
      canReverse: []
    },
    {
      id: 30,
      disk: null,
      canReverse: []
    },
    {
      id: 31,
      disk: null,
      canReverse: []
    },
    {
      id: 32,
      disk: null,
      canReverse: []
    }
  ],
  [
    {
      id: 33,
      disk: null,
      canReverse: []
    },
    {
      id: 34,
      disk: null,
      canReverse: []
    },
    {
      id: 35,
      disk: 'black',
      canReverse: []
    },
    {
      id: 36,
      disk: 'black',
      canReverse: []
    },
    {
      id: 37,
      disk: 'white',
      canReverse: []
    },
    {
      id: 38,
      disk: null,
      canReverse: [
        {
          X: 4,
          Y: 4
        },
        {
          X: 4,
          Y: 3
        }
      ]
    },
    {
      id: 39,
      disk: null,
      canReverse: []
    },
    {
      id: 40,
      disk: null,
      canReverse: []
    }
  ],
  [
    {
      id: 41,
      disk: 'white',
      canReverse: []
    },
    {
      id: 42,
      disk: 'white',
      canReverse: []
    },
    {
      id: 43,
      disk: 'white',
      canReverse: []
    },
    {
      id: 44,
      disk: 'black',
      canReverse: []
    },
    {
      id: 45,
      disk: 'white',
      canReverse: []
    },
    {
      id: 46,
      disk: null,
      canReverse: [
        {
          X: 5,
          Y: 4
        },
        {
          X: 5,
          Y: 3
        },
        {
          X: 4,
          Y: 4
        },
        {
          X: 3,
          Y: 3
        }
      ]
    },
    {
      id: 47,
      disk: null,
      canReverse: []
    },
    {
      id: 48,
      disk: null,
      canReverse: []
    }
  ],
  [
    {
      id: 49,
      disk: 'white',
      canReverse: []
    },
    {
      id: 50,
      disk: 'white',
      canReverse: []
    },
    {
      id: 51,
      disk: 'white',
      canReverse: []
    },
    {
      id: 52,
      disk: 'white',
      canReverse: []
    },
    {
      id: 53,
      disk: 'white',
      canReverse: []
    },
    {
      id: 54,
      disk: null,
      canReverse: [
        {
          X: 5,
          Y: 4
        },
        {
          X: 4,
          Y: 3
        }
      ]
    },
    {
      id: 55,
      disk: null,
      canReverse: []
    },
    {
      id: 56,
      disk: null,
      canReverse: []
    }
  ],
  [
    {
      id: 57,
      disk: 'white',
      canReverse: []
    },
    {
      id: 58,
      disk: 'white',
      canReverse: []
    },
    {
      id: 59,
      disk: 'white',
      canReverse: []
    },
    {
      id: 60,
      disk: 'white',
      canReverse: []
    },
    {
      id: 61,
      disk: 'white',
      canReverse: []
    },
    {
      id: 62,
      disk: null,
      canReverse: [
        {
          X: 6,
          Y: 4
        },
        {
          X: 5,
          Y: 3
        }
      ]
    },
    {
      id: 63,
      disk: null,
      canReverse: []
    },
    {
      id: 64,
      disk: null,
      canReverse: []
    }
  ]
]
