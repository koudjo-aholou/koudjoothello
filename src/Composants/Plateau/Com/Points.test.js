import React from 'react'
import TestRenderer from 'react-test-renderer'
import Point from './Points'

describe('Point', () => {
  it('Checked Point render correctly', async () => {
    const component = TestRenderer.create(
      <Point />
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})
