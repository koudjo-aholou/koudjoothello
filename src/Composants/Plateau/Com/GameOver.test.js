import React from 'react'
import TestRenderer from 'react-test-renderer'
import GameOver from './Pion'

describe('GameOver', () => {
  it('Checked GameOver render correctly', async () => {
    const component = TestRenderer.create(
      <GameOver />
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})
