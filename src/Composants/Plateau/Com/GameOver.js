import React, { button } from 'react'
import PropTypes from 'prop-types'
import './GameOver.scss'

function GameOver (props) {
  const gameOverTexte = () => {
    if (props.winner !== 'draw') {
      return `${props.winner} a gagné!`
    } else {
      return 'Match Nul!'
    }
  }
  return (
    <article className="GameOver">
      <h3>{gameOverTexte()}</h3>
      <p>
              Score du joueur avec les pions Blanc:
        <span className="score-text">
          {props.white}
        </span>
      </p>
      <p>
              Score du joueur avec les pions Noir
        <span className="score-text">
          {props.black}
        </span>
      </p>
      <div className='finPartie'>
        <button
          className="recoPart"
          onClick={props.restart}>
                Recommencer
        </button>
      </div>
    </article>
  )
}

GameOver.propTypes = {
  gameOverTexte: PropTypes.func,
  winner: PropTypes.string,
  white: PropTypes.number,
  black: PropTypes.number,
  restart: PropTypes.func
}

export default GameOver
