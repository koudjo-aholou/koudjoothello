import React from 'react'
import TestRenderer from 'react-test-renderer'
import Pion from './Pion'

describe('Pion', () => {
  it('Checked Pion render correctly', async () => {
    const component = TestRenderer.create(
      <Pion />
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})
