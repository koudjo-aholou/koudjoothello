import React from 'react'
import './Points.scss'
import PropTypes from 'prop-types'

export default function Points (props) {
  return (
    <>
      <div className={`Score Score--${props.player}`}>
        <span>{props.score}</span>
      </div>
      {
        props.nomJoueur ? <p className='playerName'>{props.nomJoueur}</p> : ''
      }
    </>
  )
}

Points.propTypes = {
  player: PropTypes.string,
  score: PropTypes.number,
  nomJoueur: PropTypes.string
}
