import React from 'react'
import './Pion.scss'
import PropTypes from 'prop-types'

export default function Pion (props) {
  const color = () => {
    return {
      backgroundColor: props.color
    }
  }
  return (
    <span
      className="Disk"
      style={color()}
    >
    </span>
  )
}

Pion.propTypes = {
  color: PropTypes.string
}
