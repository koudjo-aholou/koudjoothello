import React, { useState } from 'react'
import './Carre.scss'
import Pion from './Pion'
import PropTypes from 'prop-types'

export default function Carre (props) {
  const [hover, setHover] = useState(false)

  const classes = () => {
    let cls = 'Cell '
    const cell = props.data
    // console.log(cell, 'cell test ======')
    cls += cell.disk ? 'Cell--occupied' : 'Cell--vacant'
    // console.log(cell.disk, 'cell disk =====================')
    if (cell.canReverse.length) cls += ' Cell--allowed'
    if (isNewest()) cls += ' Cell--newest'

    return cls
  }

  const isNewest = () => {
    return props.newest && props.newest[0] === props.position[0] && props.newest[1] === props.position[1]
  }

  const cellContent = () => {
    return <Pion color={diskColor()} />
  }

  const setHoverState = (hovered) => {
    if (props.data.canReverse.length) {
      setHover(hovered)
    }
  }

  const diskColor = () => {
    var cell = props.data
    if (cell.disk) return cell.disk
    if (hover) return props.player

    return null
  }

  const reverse = () => {
    if (props.data.canReverse.length === 0) return

    var x = props.position[0]
    var y = props.position[1]
    props.reverse(x, y)
  }

  return (
    <td
      className={classes()}
      onMouseEnter={ () => setHoverState(true)}
      onMouseLeave={() => setHoverState(false)}
      onClick={() => reverse()}
    >
      {cellContent()}
    </td>
  )
}

Carre.propTypes = {
  reverse: PropTypes.func,
  data: PropTypes.object,
  position: PropTypes.array,
  player: PropTypes.string,
  newest: PropTypes.array
}
