import React from 'react'
import Carre from './Carre.js'
import './Plateau.scss'
import PropTypes from 'prop-types'

export default function Plateau (props) {
  const renderRow = (row, x) => {
    return row.map((cell, y) => <Carre key={y} data={cell} newest={props.newest} reverse={props.reverse} player={props.player} position={[x, y]}/>)
  }

  const repereLettre = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

  return (
    <div className="Board">
      {// table table-bordered css
      }
      <table className="gameboard">
        <thead>
          <tr>
            {
              repereLettre.map((lettre, index) => {
                return <th scope="col" key={index} className='repere'>{lettre}</th>
              })
            }
          </tr>
        </thead>
        <tbody>
          { props.board.map((row, x) => <tr key={x}>{renderRow(row, x)}</tr>) }
        </tbody>
        <tfoot>
          <tr>
            {
              repereLettre.map((lettre, index) => {
                return <th scope="col" key={index} className='repere'>{lettre}</th>
              })
            }
          </tr>
        </tfoot>
      </table>
    </div>
  )
}

Plateau.propTypes = {
  reverse: PropTypes.func,
  board: PropTypes.array,
  data: PropTypes.object,
  position: PropTypes.array,
  player: PropTypes.string,
  newest: PropTypes.array
}
