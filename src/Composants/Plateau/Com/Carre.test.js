import React from 'react'
import TestRenderer from 'react-test-renderer'
import Carre from './Carre'

describe('Carre', () => {
  it('Checked Carre render correctly', async () => {
    const y = 0
    const cell = { id: 1, disk: 'black', canReverse: [] }
    const newest = []
    const reverse = jest.fn(2, 4)
    const player = 'Koudjo'
    const position = [3, 2]

    const component = TestRenderer.create(
      <Carre key={y} data={cell} newest={newest} reverse={reverse} player={player} position={position} />
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})
