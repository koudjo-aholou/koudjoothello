import React from 'react'
import TestRenderer from 'react-test-renderer'
import Jeu from './Jeu-state'

describe('Jeu-state', () => {
  it('Checked Jeu-state render correctly', async () => {
    const component = TestRenderer.create(
      <Jeu />
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})

// Test Diagonale && Horizontal && Vertical
// initialDisk (x, y) {
//   if ((x === 2 && y === 2) || (x === 2 && y === 1) || (x === 1 && y === 2) || (x === 8 && y === 1)) return 'black'
//   if ((x === 1 && y === 1) || (x === 3 && y === 2) || (x === 5 && y === 1)) return 'white'
//   return null
// }

// Test diagonal horizontal et vertical en meme temps
// initialDisk (x, y) {
//   if ((x === 8 && y === 1) || (x === 3 && y === 1) || (x === 3 && y === 2) || (x === 4 && y === 2)) return 'black'
//   if ((x === 0 && y === 1) || (x === 2 && y === 1) || (x === 2 && y === 3) || (x === 4 && y === 3)) return 'white'
//   return null
// }

// Test coin ne remplace pion que si pion posé décisif ds l action
// initialDisk (x, y) {
//   if ((x === 7 && y === 1) || (x === 8 && y === 2) || (x === 7 && y === 2)) return 'black'
//   if ((x === 6 && y === 1) || (x === 1 && y === 8) || (x === 8 && y === 3)) return 'white'
//   return null
// }
