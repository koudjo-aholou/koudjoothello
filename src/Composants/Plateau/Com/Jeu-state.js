import React, { Component } from 'react'
import Plateau from './Plateau'
import Points from './Points'
import './Jeu.scss'
import PropTypes from 'prop-types'
import axios from 'axios'

const identification = localStorage.getItem('token')

// orientation ds le tableau
const directions = [
  [0, 1], // droite
  [0, -1], // gauche
  [-1, 0], // haut
  [1, 0], // bas
  [1, 1], // diagonale - bas droite
  [-1, 1], // diagonale - haut droite
  [-1, -1], // diagonale - haut gauche
  [1, -1] // diagonale - bas gauche
]
// idGame

class Jeu extends Component {
  constructor (props) {
    super(props)
    this.state = {
      board: this.createBoard(),
      boardServeur: [],
      currentPlayer: 'black',
      errStat: '',
      identification: localStorage.getItem('token'),
      winner: null,
      lostTurn: false,
      newestDisk: null,
      endPassTurn: {
        playerOne: false,
        playerTwo: false,
        endGame: false
      }
    }
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevState.currentPlayer !== this.state.currentPlayer) {
      this.saveScoreEtPlateau()
    }
  }

  componentDidMount () {
    this.calculateAllowedCells()
    this.fetchData()
  }

  async fetchData () {
    const formData = {
      _id: this.props.idGame
    }

    const config = {
      headers: {
        'Content-Type': 'application/json',
        'X-Auth-Token': identification
      }
    }

    await axios.post('http://localhost:5000/api/games/game', formData, config)
      .then(res => {
        const scoreP1 = res.data.game.score.playerone
        const scoreP2 = res.data.game.score.playertwo
        const getPlateau = res.data.game.board

        this.setState({ boardServeur: getPlateau })
        if (scoreP1 === 2 && scoreP2 === 2) {
          // console.log('nouveau jeu')
        } else {
          this.setState({ board: res.data.game.board })
          this.setState({ currentPlayer: res.data.game.score.currentPlayer })
        }
        // console.log(res.data.game.board)
        // console.log(scoreP1, scoreP2)
      })
      .catch(err => this.setState({ errStat: err }))
  }

  vainqueurPartie () {
    const scorePionsBlancs = this.score('white')
    const scorePionsNoirs = this.score('black')

    if (scorePionsBlancs > scorePionsNoirs) return 'white'
    if (scorePionsBlancs === scorePionsNoirs) return 'draw'
    return 'black'
  }

  tourJoueurPerdu () {
    if (this.state.lostTurn) {
      return alert(`${this.couleurPionAdversaire()} doit passer son tour`)
    } else {
      return ''
    }
  }

  score (player) {
    let score = 0
    this.state.board.forEach(row => {
      row.forEach(cell => {
        if (cell.disk === player) score++
      })
    })
    return score
  }

  async saveScoreEtPlateau () {
    try {
      const formData = {
        _id: this.props.idGame,
        score: {
          playerone: this.score('black'),
          playertwo: this.score('white'),
          currentPlayer: this.state.currentPlayer
        },
        board: this.state.board
      }

      const config = {
        headers: {
          'Content-Type': 'application/json',
          'X-Auth-Token': identification
        }
      }

      await axios.put('http://localhost:5000/api/games/game', formData, config)
        .then(res => {
        //  console.log(res)
        })
        .catch(err => console.log(err))
    } catch (err) {
      console.error(err.message)
    }
  }

  hasOpponentsColor (x, y) {
    return this.state.board[x][y].disk === this.couleurPionAdversaire()
  }

  couleurPionAdversaire () {
    return this.state.currentPlayer === 'white' ? 'black' : 'white'
  }

  iPassMyTurn (joueur, playerOnePassTurn1, playerTwoPassTurn1) {
    if (this.state.endPassTurn.playerTwo === true) {
      this.checkEndGamePass(joueur)
    }
    if (this.state.endPassTurn.endGame === true) {
      this.checkEndGamePass(joueur)
    }

    if (joueur === 'white') {
      const status = {
        playerOne: false,
        playerTwo: true,
        endGame: false
      }
      this.setState({ currentPlayer: 'black' })
      this.setState({ endPassTurn: status })
    } else {
      const status = {
        playerOne: true,
        playerTwo: false,
        endGame: true
      }
      this.setState({ currentPlayer: 'white' })
      this.setState({ endPassTurn: status })
    }
  }

  checkEndGamePass (joueur) {
    // console.log('fin du jeu ==========================================================', 'joueur')
    this.props.end(this.vainqueurPartie(), this.score('white'), this.score('black'))
  }

  render () {
    let whichPlayer
    if (this.state.currentPlayer === 'black') {
      whichPlayer = this.props.playerName
    } else {
      whichPlayer = ` l'ordinateur pion ${this.state.currentPlayer}`
    }
    return (
      <section className="Game container">
        {this.tourJoueurPerdu()}
        <article className="row">
          <div className="rowTurn">
            <Plateau
              board={this.state.board}
              newest={this.state.newestDisk}
              reverse={ (x, y) => this.reverse(x, y)}
              player={this.state.currentPlayer}
            />
            <aside className="turnAndPass">
              <div className="tabScore">
                <Points
                  player="black"
                  score={this.score('black')}
                  nomJoueur={this.props.playerName}
                />
                <Points
                  player="white"
                  score={this.score('white')}
                />
              </div>
              <h3 className="Game--title"> Au tour de  {whichPlayer}</h3>
              <button
                onClick={() => this.iPassMyTurn(this.state.currentPlayer, this.state.playerOnePassTurn1, this.state.playerTwoPassTurn1)} className="passTurn"
              >
                Passer son tour
              </button>
            </aside>
          </div>
        </article>
      </section>)
  }

  calculateAllowedCells () {
    var b = this.state.board
    var allowedCellsCount = 0
    var canReverse

    for (let x = 0; x < 8; x++) {
      for (let y = 0; y < 8; y++) {
        canReverse = this.canReverse(x, y)
        b[x][y].canReverse = canReverse

        if (canReverse.length) allowedCellsCount++
      }
    }

    this.setState({
      board: b
    })

    return allowedCellsCount
  }

  // Plus besoin initialisé par le serveur si pas début de partie
  createBoard () {
    const board = new Array(8)
    let rowPos
    for (let x = 0; x < board.length; x++) {
      board[x] = new Array(8)
      rowPos = x * 8
      for (let y = 0; y < board[x].length; y++) {
        board[x][y] = {
          id: rowPos + (y + 1),
          disk: this.initialDisk(x + 1, y + 1),
          canReverse: []
        }
      }
    }

    return board
  }

  // PLus besoin initialisé par le serveur

  /** initit plateau noir : 4,4;5,5 et joueur blanc a 4,5; 5,4; */
  initialDisk (x, y) {
    if ((x === 4 && y === 4) || (x === 5 && y === 5)) return 'black'
    if ((x === 4 && y === 5) || (x === 5 && y === 4)) return 'white'
    return null
  }

  canReverse (x, y) {
    var canReverse = []
    var b = this.state.board
    var X, Y, distance, cells

    // case occupé
    if (b[x][y].disk) return []

    directions.forEach(dir => {
      distance = 0
      X = x
      Y = y
      cells = []

      do {
        X += dir[0]
        Y += dir[1]
        cells.push({ X, Y })
        distance++
      } while (this.inBoard(X, Y) && this.hasOpponentsColor(X, Y))

      if (distance >= 2 && this.inBoard(X, Y) && b[X][Y].disk === this.state.currentPlayer) {
        canReverse.push(cells)
      }
    })

    return [].concat.apply([], canReverse)
  }

  inBoard (x, y) {
    return x >= 0 && x <= 7 && y >= 0 && y <= 7
  }

  reverse (x, y) {
    //  console.log('joueur jou maintenant')
    var b = this.state.board

    if (!b[x][y].canReverse || !b[x][y].canReverse.length) return

    b[x][y].disk = this.state.currentPlayer
    // eslint-disable-next-line no-return-assign
    b[x][y].canReverse.forEach(cell => b[cell.X][cell.Y].disk = this.state.currentPlayer)
    this.setState({
      board: b,
      newestDisk: [x, y]
    }, () => {
      this.setState((prevState) => {
        return {
          currentPlayer: prevState.currentPlayer === 'white' ? 'black' : 'white'
        }
      }, () => {
        // var car réutiliser plusieurs fois dans le scope du module
        var allowedCellsCount = this.calculateAllowedCells()
        //  console.log(allowedCellsCount, 'check')
        if (!allowedCellsCount) { // Pas de poss de jouer
          this.setState((prevState) => {
            return {
              currentPlayer: prevState.currentPlayer === 'white' ? 'black' : 'white'
            }
          }, () => {
            allowedCellsCount = this.calculateAllowedCells()
            if (!allowedCellsCount) { // 2 joueurs ne peuvet pas jouer fin partie
              this.props.end(this.vainqueurPartie(), this.score('white'), this.score('black'))
            }
          })
        }
      })
    })
  }

  getCurrentPlayer () {
    // verif si joeur peut jouer en comptant cellules
    var allowedCellsCount = this.calculateAllowedCells()

    if (!allowedCellsCount) {
      this.setState({
        lostTurn: true
      })

      return this.state.currentPlayer
    }

    return this.state.currentPlayer === 'white' ? 'black' : 'white'
  }
}

Jeu.propTypes = {
  end: PropTypes.func,
  idGame: PropTypes.string,
  playerName: PropTypes.string
}

export default Jeu
