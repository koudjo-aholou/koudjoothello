import React from 'react'
import TestRenderer from 'react-test-renderer'
import Plateau from './index'
import {
  BrowserRouter as Router
} from 'react-router-dom'

describe('Plateau', () => {
  it('Checked Plateau render correctly', async () => {
    const component = TestRenderer.create(
      <Router>
        <Plateau />
      </Router>
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})
