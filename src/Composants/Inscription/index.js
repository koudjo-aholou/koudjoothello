import React, { useState } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
// import Cookies from 'universal-cookie'

/* error Extra semicolon. */

function Inscription () {
  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')
  const [erreur, setErreur] = useState(false)
  const [identification, setIdentification] = useState(false)
  const [passwordIns, setPasswordIns] = useState('')
  const [emailIns, setEmailIns] = useState('')
  const [nameIns, setNameIns] = useState('')

  const handleConnect = (e) => {
    e.preventDefault()
    const url = 'http://localhost:5000/api/auth'
    axios.post(url, {
      email: email,
      password: password
    })
      .then(function (response) {
        const tokenUser = response.data.token
        // sessionStorage.setItem('token', tokenUser)
        localStorage.setItem('token', tokenUser)
        setErreur(false)
        setIdentification(true)
      })
      .catch(function (error) {
        console.log(error)
        refresh()
        setErreur(true)
        setIdentification(false)
      })

    setPassword('')
    setEmail(' ')
  }

  const handleInscription = (e) => {
    e.preventDefault()
    const url = 'http://localhost:5000/api/users'
    axios.post(url, {
      name: nameIns,
      email: emailIns,
      password: passwordIns
    }).then(function (response) {
      const tokenUser = response.data.token
      // sessionStorage.setItem('token', tokenUser)
      localStorage.setItem('token', tokenUser)
      setErreur(false)
      setIdentification(true)
    })
      .catch(function (error) {
        console.log(error)
        setErreur(true)
        refresh()
        setIdentification(false)
      })
    setPassword('')
    setEmail(' ')
    setNameIns('')
  }

  const refresh = () => {
    if (setErreur) {
      window.location.reload()
    }
  }

  return (
    <div className="App">
      <main className="App-header">
        { erreur === true ? <p>ERREUR CONNECTION/INSCRIPTION</p> : ''}
        <h1>Vous devez vous connecter ou vous inscrire</h1>
        <h2>Connection</h2>
        <form className="container" onSubmit={handleConnect}>
          <label className="connectLabel">
            <span className="espaceDroit">E-mail</span>
            <input type="text" name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
          </label>
          <label>
            <span className="espaceGauche espaceDroit">Password</span>
            <input type="text" name="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
          </label>
          <button
            onClick={(e) => handleConnect(e)}
            className='connectBtn'
          >
            <p>Se connecter</p>
          </button>
          { identification === true ? <Redirect to={'/'} /> : ''}
        </form>

        <h3>Inscription</h3>
        <form className="container" onSubmit={handleConnect}>
          <label>
            <span className="espaceDroit">Nom*</span>
            <input type="text" name="nom" value={nameIns} onChange={(e) => setNameIns(e.target.value)} />
          </label>
          <label>
            <span className="espaceGauche espaceDroit">E-mail* (au format xx@nomdedomaine.com et sensible majuscule)</span>
            <input type="text" name="emailIns" value={emailIns} onChange={(e) => setEmailIns(e.target.value)} />
          </label>
          <label>
            <span className="espaceGauche espaceDroit">Password* (min 1 caractere)</span>
            <input type="text" name="password" value={passwordIns} onChange={(e) => setPasswordIns(e.target.value)}/>
          </label>
          <button
            onClick={(e) => handleInscription(e)}
            className='connectBtn'
          >
            <p>S Inscrire</p>
          </button>
          { identification === true ? <Redirect to={'/'} /> : ''}
        </form>
      </main>
    </div>
  )
}

export default Inscription
