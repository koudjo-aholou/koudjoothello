import React from 'react'
import TestRenderer from 'react-test-renderer'
import Inscription from './index'

describe('Home', () => {
  it('Checked Home render correctly', async () => {
    const component = TestRenderer.create(
      <Inscription />
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})
