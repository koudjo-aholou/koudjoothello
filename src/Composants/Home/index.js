import React, { useState, useEffect, useRef } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

function Home () {
  const [identification, setIdentification] = useState(localStorage.getItem('token'))
  const [status, setStatus] = useState(false)
  const [idJeu, setIdJeu] = useState([])

  const [dataUser, setDataUser] = useState('')
  const didMountRef = useRef(false)

  async function fetchData () {
    setIdentification(localStorage.getItem('token'))
    if (identification) {
      await axios('http://localhost:5000/api/auth', {
        headers: {
          'X-Auth-Token': identification
        }
      })
        .then(res => {
          setDataUser(res.data.user)
          setIdJeu(res.data.user.jeux_id)
        })
        .catch(err => setStatus(err))
    }
  }

  useEffect(() => {
    if (!didMountRef.current) {
      didMountRef.current = true
      fetchData()
    }// react-hooks/exhaustive-deps linter
  }, []) // eslint-disable-line 

  function signOut () {
    localStorage.removeItem('token')
    setDataUser('')
    setIdentification('')
  }

  function creerNouvjeu () {
    const formData = {
      score: {
        playerone: 2,
        playertwo: 2,
        currentPlayer: 'black'
      },
      playerone: dataUser._id,
      board: [
        [
          [
            {
              id: 1,
              disk: null,
              canReverse: []
            },
            {
              id: 2,
              disk: null,
              canReverse: []
            },
            {
              id: 3,
              disk: null,
              canReverse: []
            },
            {
              id: 4,
              disk: null,
              canReverse: []
            },
            {
              id: 5,
              disk: null,
              canReverse: []
            },
            {
              id: 6,
              disk: null,
              canReverse: []
            },
            {
              id: 7,
              disk: null,
              canReverse: []
            },
            {
              id: 8,
              disk: null,
              canReverse: []
            }
          ],
          [
            {
              id: 9,
              disk: null,
              canReverse: []
            },
            {
              id: 10,
              disk: null,
              canReverse: []
            },
            {
              id: 11,
              disk: null,
              canReverse: []
            },
            {
              id: 12,
              disk: null,
              canReverse: []
            },
            {
              id: 13,
              disk: null,
              canReverse: []
            },
            {
              id: 14,
              disk: null,
              canReverse: []
            },
            {
              id: 15,
              disk: null,
              canReverse: []
            },
            {
              id: 16,
              disk: null,
              canReverse: []
            }
          ],
          [
            {
              id: 17,
              disk: null,
              canReverse: []
            },
            {
              id: 18,
              disk: null,
              canReverse: []
            },
            {
              id: 19,
              disk: null,
              canReverse: []
            },
            {
              id: 20,
              disk: null,
              canReverse: []
            },
            {
              id: 21,
              disk: null,
              canReverse: []
            },
            {
              id: 22,
              disk: null,
              canReverse: []
            },
            {
              id: 23,
              disk: null,
              canReverse: []
            },
            {
              id: 24,
              disk: null,
              canReverse: []
            }
          ],
          [
            {
              id: 25,
              disk: null,
              canReverse: []
            },
            {
              id: 26,
              disk: null,
              canReverse: []
            },
            {
              id: 27,
              disk: null,
              canReverse: []
            },
            {
              id: 28,
              disk: 'black',
              canReverse: []
            },
            {
              id: 29,
              disk: 'white',
              canReverse: []
            },
            {
              id: 30,
              disk: null,
              canReverse: []
            },
            {
              id: 31,
              disk: null,
              canReverse: []
            },
            {
              id: 32,
              disk: null,
              canReverse: []
            }
          ],
          [
            {
              id: 33,
              disk: null,
              canReverse: []
            },
            {
              id: 34,
              disk: null,
              canReverse: []
            },
            {
              id: 35,
              disk: null,
              canReverse: []
            },
            {
              id: 36,
              disk: 'white',
              canReverse: []
            },
            {
              id: 37,
              disk: 'black',
              canReverse: []
            },
            {
              id: 38,
              disk: null,
              canReverse: []
            },
            {
              id: 39,
              disk: null,
              canReverse: []
            },
            {
              id: 40,
              disk: null,
              canReverse: []
            }
          ],
          [
            {
              id: 41,
              disk: null,
              canReverse: []
            },
            {
              id: 42,
              disk: null,
              canReverse: []
            },
            {
              id: 43,
              disk: null,
              canReverse: []
            },
            {
              id: 44,
              disk: null,
              canReverse: []
            },
            {
              id: 45,
              disk: null,
              canReverse: []
            },
            {
              id: 46,
              disk: null,
              canReverse: []
            },
            {
              id: 47,
              disk: null,
              canReverse: []
            },
            {
              id: 48,
              disk: null,
              canReverse: []
            }
          ],
          [
            {
              id: 49,
              disk: null,
              canReverse: []
            },
            {
              id: 50,
              disk: null,
              canReverse: []
            },
            {
              id: 51,
              disk: null,
              canReverse: []
            },
            {
              id: 52,
              disk: null,
              canReverse: []
            },
            {
              id: 53,
              disk: null,
              canReverse: []
            },
            {
              id: 54,
              disk: null,
              canReverse: []
            },
            {
              id: 55,
              disk: null,
              canReverse: []
            },
            {
              id: 56,
              disk: null,
              canReverse: []
            }
          ],
          [
            {
              id: 57,
              disk: null,
              canReverse: []
            },
            {
              id: 58,
              disk: null,
              canReverse: []
            },
            {
              id: 59,
              disk: null,
              canReverse: []
            },
            {
              id: 60,
              disk: null,
              canReverse: []
            },
            {
              id: 61,
              disk: null,
              canReverse: []
            },
            {
              id: 62,
              disk: null,
              canReverse: []
            },
            {
              id: 63,
              disk: null,
              canReverse: []
            },
            {
              id: 64,
              disk: null,
              canReverse: []
            }
          ]
        ]
      ]

    }
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'X-Auth-Token': identification
      }
    }

    axios.post('http://localhost:5000/api/games', formData, config)
      .then(function (response) {
      // redirect vers la page program index
        //  console.log(response.data.game._id, 'JEUX CREE =======')
        addGameToPlayer(response.data.game._id)
      })
      .catch(function (error) {
        console.log(error)
      })
  }

  function addGameToPlayer (gameId) {
    const formData = {
      jeux_id: gameId,
      _id: dataUser._id
    }

    const config = {
      headers: {
        'Content-Type': 'application/json',
        'X-Auth-Token': identification
      }
    }

    axios.put('http://localhost:5000/api/users', formData, config)
      .then(function (response) {
        // redirect vers la page program index
        // console.log(response, 'JEU LIEE AU JOUEUR', response.data.data.jeux_id)
        fetchData()
      })
      .catch(function (error) {
        console.log(error)
      })
  }

  return (
    <div className="App">
      <main className="App-header">
        {
          identification
            // eslint-disable-next-line react/no-unescaped-entities
            ? <>
              <nav className="deconnect">
                <button
                  className='deconnectBtn'
                  onClick={() => signOut() }
                >
                  Se deconnecter
                </button>
              </nav>
              {status !== false ? <p>Il y a une erreur de Token</p> : ''}
              <h1>
                Bonjour {dataUser.name} ! Vous avez {idJeu.length} parties
              </h1>
              <div>
                <button
                  className="HomeBtn"
                  onClick={ () => creerNouvjeu() }
                >
                Creer une nouvelle partie
                </button>
              </div>

              <ul className="liste-des-jeux">
                {
                  idJeu.map((jeu) => (
                    <li key={jeu._id}>
                      <Link
                        to={`/plateau/${jeu._id}`}
                      >
                      Rejoindre cette partie <span className="partieTitle"> {jeu._id}</span>
                      </Link>
                    </li>
                  ))
                }
              </ul>
            </>
            : <>
              <h1>Jouer au Othello !</h1>
              <div className='containerNotCo'>
                <Link className="connectBtn" to='/inscription'> Vous devez etre connecte pour jouer</Link>
              </div>
            </>
        }
      </main>
    </div>
  )
}

export default Home
