import React from 'react'
import TestRenderer from 'react-test-renderer'
import Home from './index'
import {
  BrowserRouter as Router
} from 'react-router-dom'

describe('Home', () => {
  it('Checked Home render correctly', async () => {
    const component = TestRenderer.create(
      <Router>
        <Home />
      </Router>
    )
    const result = component.toJSON()
    expect(result).toMatchSnapshot()
  })
})
