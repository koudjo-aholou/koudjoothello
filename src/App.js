import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

import Home from './Composants/Home/index'
import Inscription from './Composants/Inscription/index'
import Plateau from './Composants/Plateau/index'

import './Styles/App.scss'

function App () {
  return (
    <Router>
      <div className='logo'>
        <img src ='https://www.zenika.com/static/logos/znk.png' alt='logo zenika'/>
        <Link className='headerTitle' to="/">Home</Link>
        <Link className='headerTitle' to="/inscription">Inscription</Link>
      </div>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/inscription" component={Inscription} />
        <Route path="/plateau/:id" component={Plateau} />
      </Switch>
    </Router>

  )
}

export default App
// <Link to="/plateau/demo">Plateau</Link>
