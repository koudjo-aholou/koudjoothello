OTHELLO REACT


## A LIRE
L'application se lance sur le port 9000 qui peut être changé si nécessaire dans .env

La BDD est NoSQL avec express et mongoose
La variable d environnement permettant de lier sa base de donnée avec Mlab ou mongoAtlas se trouve dans  \serveur\config\default.json <br />
Il est nécesseraire de modifier dans le fichier default.json : la propriété mongoURI comme ci dessous : <br />
Dans le cas d utilisation de mlab l'url est : 
mongodb://<nomutilisateur>:<motdepasseutilisateur>@ds135820.mlab.com:35820/othello-koudjo

Il est également possible de modifier le mot de passe du jsonwebtoken en modifiant la propriété "jwtSecret" avec une String <br />

Le token est envoyé par le serveur et est stocké dans le localStorage à la connection ou à l'inscription <br />

le fichier exam-test-route.postman_collection.json permet de afficher ou ajouter des datas en BDD si nécessaire <br />

## Structure  des dossiers dans dossier serveur
  config : acces bdd et variables d'env <br />
  routes\api : dossier des routes (cf : exam-test-route.postman_collection.json) <br />
  models : 2 modeles : Games et Users <br />
  middleware : le fichier auth permet de vérifier le token <br />

## Scripts disponibles

### npm start
Lance l'application [http://localhost:9000]


### npm run lint
Lance le linter

### npm test
Lance les tests

### npm run watchtest
Lance les tests avec refresh automatique

### npm test -- --coverage --watchAll=false
Coverage de test

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


